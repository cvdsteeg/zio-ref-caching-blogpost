import Dependencies._

enablePlugins(ScalafmtPlugin)

name := "zio-ref-caching-blogpost"
organization := "com.jdriven"
version := "v1"
scalaVersion := "2.13.1"
version := sys.env.getOrElse("CI_PIPELINE_ID", "local-SNAPSHOT")

mainClass in Compile := Some("com.jdriven.cvdsteeg.zio.Main")

resolvers += Resolver.sonatypeRepo("releases")

addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3")
addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1")

libraryDependencies ++= logging ++ zio ++ pureConfig ++ Dependencies.test

testFrameworks ++= Seq(new TestFramework("zio.test.sbt.ZTestFramework"))
