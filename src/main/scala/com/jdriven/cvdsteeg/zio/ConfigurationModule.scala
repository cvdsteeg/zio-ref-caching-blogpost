package com.jdriven.cvdsteeg.zio

import java.util.concurrent.TimeUnit

import pureconfig._
import pureconfig.generic.auto._
import zio._
import zio.clock.Clock
import zio.console._
import zio.duration._

trait ConfigurationModule {
  val configurationModule: ConfigurationModule.Service[Any]
}

object ConfigurationModule {

  case class ConfigurationError(message: String) extends RuntimeException(message)
  case class Configuration(appName: String)

  trait Service[R] {
    def configuration: ZIO[R, Throwable, Configuration]
  }

  trait Live extends ConfigurationModule {
    val clock: Clock.Service[Any]
    val console: Console.Service[Any]
    val configRef: Ref[Option[Configuration]]

    val configurationModule: ConfigurationModule.Service[Any] = new Service[Any] {
      override def configuration: Task[Configuration] = useConfigFromRef.orElse(loadHeavyConfigFromFile)
    }

    private def useConfigFromRef =
      for {
        configOption <- configRef.get
        config       <- ZIO.fromOption(configOption).mapError(_ => ConfigurationError("The config isn't loaded"))
        _            <- console.putStrLn("The configuration was already loaded. So convenient!")
      } yield config

    private def loadHeavyConfigFromFile =
      for {
        (duration, config) <- ZIO
          .fromEither(ConfigSource.default.load[Configuration])
          .mapError(e => ConfigurationError(e.toList.mkString(", ")))
          .delay(500.milliseconds)
          .timed
          .provide(Clock.Live)
        _ <- console.putStrLn(s"Loaded configuration from file. That took ${duration.toMillis} ms")
        _ <- configRef.update(_ => Some(config))
      } yield config
  }

  object factory extends ConfigurationModule.Service[ConfigurationModule] {
    override def configuration: ZIO[ConfigurationModule, Throwable, Configuration] = ZIO.accessM[ConfigurationModule](_.configurationModule.configuration)
  }
}
