package com.jdriven.cvdsteeg.zio

import com.jdriven.cvdsteeg.zio.ConfigurationModule.Configuration
import zio._
import zio.clock.Clock
import zio.console.Console

object Main extends App {
  def run(args: List[String]): ZIO[ZEnv, Nothing, Int] = program

  val logic: ZIO[Console with ConfigurationModule, Nothing, Int] = (for {
    _      <- console.putStrLn(s"I'm running!")
    config <- ConfigurationModule.factory.configuration
    _      <- console.putStrLn(s"This is the ${config.appName} application")
    config <- ConfigurationModule.factory.configuration
    _      <- console.putStrLn(s"Again, this is the ${config.appName} application")
    config <- ConfigurationModule.factory.configuration
    _      <- console.putStrLn(s"And again, this is still the ${config.appName} application")
  } yield 0)
    .catchAll(e => console.putStrLn(s"Application run failed $e").as(1))

  private val program = for {
    ref <- Ref.make[Option[Configuration]](None)
    logic <- logic.provideSome[Console with Clock] { c =>
      new Clock with Console with ConfigurationModule.Live {
        override val clock: Clock.Service[Any]             = c.clock
        override val console: Console.Service[Any]         = c.console
        override val configRef: Ref[Option[Configuration]] = ref
      }
    }
  } yield logic
}
