package com.jdriven.cvdsteeg.zio

import zio.test._
import zio.test.Assertion._

object MainTest
    extends DefaultRunnableSpec(
      suite("Main")(
        testM("should successfully complete") {
          for {
            result <- Main.run(List())
          } yield assert(result, equalTo(0))
        }
      )
    )
